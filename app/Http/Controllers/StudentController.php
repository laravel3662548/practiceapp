<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $studentData = Student::get();
        return view('student/index', compact('studentData'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('student/create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required','string','min:3','max:100'],
            'age' => ['required','string','max:2'],
            'class' => ['required','string','max:10']
        ]);

        
        Student::create([
            'name' => $request->name,
            'age' => $request->age,
            'class' => $request->class
        ]);

        //dd($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student,$s_id)
    {
        $studentData = $student->where('id',$s_id)->first();
        //dd($studentData);
        return view('student/edit', compact("studentData"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Student $student, $s_id)
    {
        $request->validate([
            'name' => ['required'],
            'age' => ['required','max:2'],
            'class' => ['required','max:10']
        ]);
        $update = $student->where('id',$s_id)->update([
            'name' => $request->name,
            'age' => $request->age,
            'class' => $request->class
        ]);
        // dd($update);
        if($update)
            return back()->with('success', 'Record updated successfully');
        else
            return back()->with('error', 'Record not found');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student, $s_id)
    {
        $get = $student->where('id', $s_id)->delete();

        if($get)
        {
            return back()->with('success', 'Record deleted successfully.');
        }
        else
        {
            return back()->with('error', 'Record not found.');
        }
        //dd($tt);
        
    }
}
