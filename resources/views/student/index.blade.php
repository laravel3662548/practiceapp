<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Create</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        li{
            list-style: none;
            float:left;
            margin-right:5px;
        }
    </style>
</head>
<body>
    <div class="container mt-3">
        <h2>Dark Striped Table</h2>
        <p>Combine .table-dark and .table-striped to create a dark, striped table:</p>     
        
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif
        <table class="table table-dark table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Age</th>
              <th>Class</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($studentData as $data)
            <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->name}}</td>
                <td>{{$data->age}}</td>
                <td>{{$data->class}}</td>
                <td>
                    <ul>
                        <li><a href="{{route('student.edit', $data->id)}}" class="btn btn-success btn-sm">Edit</a></li>
                        <li>    
                            <form action="{{route('student.delete', $data->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
</body>
</html>