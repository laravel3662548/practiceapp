<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Create</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container mt-3">
        <h2>Stacked form</h2>
        <form method="post" action="{{route('student.store')}}">
            @csrf
            <div class="mb-3 mt-3">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
            </div>
            <div class="mb-3 mt-3">
                <label for="age">Age:</label>
                <input type="text" class="form-control" id="age" placeholder="Enter age" name="age">
            </div>
            <div class="mb-3 mt-3">
                <label for="class">Class:</label>
                <input type="text" class="form-control" id="class" placeholder="Enter class" name="class">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{route('student.index')}}" class="btn btn-success">Student Data</a>
        </form>
    </div>
</body>
</html>