<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Create</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container mt-3">
        <h2>Student Information Edit Form</h2>
        
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif

        <form method="post" action="{{route('student.update', $studentData->id)}}">
            @csrf
            @method('patch')
            <div class="mb-3 mt-3">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" value="{{$studentData->name}}" name="name">
            </div>
            <div class="mb-3 mt-3">
                <label for="age">Age:</label>
                <input type="text" class="form-control" id="age" value="{{$studentData->age}}" name="age">
            </div>
            <div class="mb-3 mt-3">
                <label for="class">Class:</label>
                <input type="text" class="form-control" id="class" value="{{$studentData->class}}" name="class">
            </div>
            <button type="submit" class="btn btn-secondary">Update</button>
            <a href="{{route('student.index')}}" class="btn btn-success">Student Data</a>
        </form>
    </div>
</body>
</html>